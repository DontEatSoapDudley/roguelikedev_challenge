#include "player.hh"
#include "libtcod.hpp"

// Simple constructor that will change as the game gets more advanced
Player::Player()
{
    character = '@';
    alive = true;
    health = 100;
    x = 40;
    y = 25;
}


void Player::handleKeys(TCOD_key_t &key)
{
    if(key.vk == TCODK_UP)
        moveUp();
    else if(key.vk == TCODK_DOWN)
        moveDown();
    else if(key.vk == TCODK_RIGHT)
        moveRight();
    else if(key.vk == TCODK_LEFT)
        moveLeft();
}