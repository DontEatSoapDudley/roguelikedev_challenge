#include "libtcod.hpp"
#include "engine.hh"

int main(void)
{
    Engine engine;
    engine.init();
    // the main driver loop
    while(engine.isRunning() && !TCODConsole::isWindowClosed())
    {
        // handle events, update, draw: if you're having issues with your character not updating or things not drawing
        // check to make sure your loop is in the right order!
        engine.handleKeys();
        engine.update();
        engine.draw();
        
    }

    return 0;
}
