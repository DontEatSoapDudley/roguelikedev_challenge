#include "engine.hh"
#include "libtcod.hpp"

void Engine::init()
{
    TCODConsole::initRoot(SCREENWIDTH, SCREENHEIGHT, "roguelikedev challenge 0.1", false);
    TCODSystem::setFps(FPS);
    running = true;
}

void Engine::draw()
{
    TCODConsole::root->clear();
    player.draw();
    TCODConsole::root->flush();
}

void Engine::handleKeys()
{
    key = TCODConsole::checkForKeypress();
    if(key.vk == TCODK_ESCAPE)
        running = false;
    player.handleKeys(key);
}

void Engine::update()
{
    // nothing to update, so nothing for now.
}

