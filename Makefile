CC = g++
TARGET = roguelike_challenge 
LIBS = -L. -ltcod -ltcodgui -Wl,-rpath=.
CFLAGS = -Iinclude -Isrc -Wall -Wextra -Werror -std=c++14
DBG = -g

.PHONY: default all clean

default: $(TARGET)

Debug: $(TARGET)

Release: $(TARGET)

all: default

SOURCES = $(wildcard *.cc) $(wildcard */*.cc)
HEADERS = $(wildcard *.hh) $(wildcard */*.hh)
OBJECTS = $(SOURCES:.cc=.o)

%.o: %.cc $(HEADERS)
	$(CC) $(CFLAGS) $(DBG) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) $(LIBS) -o $@

clean:
	rm -rf $(OBJECTS) $(TARGET)
