#pragma once
#include "libtcod.hpp"

// A general entity class that will be the root of all *entities* in our game
class Entity
{
public:
    /* Virtual so we can ovveride this function for special entities that may need 
     * a special draw function */
    virtual void draw()
    {
        TCODConsole::root->putChar(x, y, character);
    }
    /** These movement functions are virtual because some characters might require special
     * movement logic */
    virtual void moveDown()  { y++; }
    virtual void moveUp()    { y--; }
    virtual void moveLeft()  { x--; }
    virtual void moveRight() { x++; }

protected:
    int x, y;
    int health;
    bool alive;
    char character;
    
};