#pragma once
#include "entity.hh"
#include "libtcod.hpp"

// Our hero. Again, not much to see yet
class Player : public Entity
{
public:
    Player();
    void handleKeys(TCOD_key_t &key);
private:

};