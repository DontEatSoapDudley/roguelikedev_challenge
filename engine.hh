#pragma once
#include "player.hh"
#include "libtcod.hpp"
#define SCREENWIDTH 80
#define SCREENHEIGHT 50
#define FPS 60

// Most of the game logic will be used through here, but there's nothing much to show yet
class Engine
{
public:
    void init();
    void draw();
    void handleKeys();
    void update();
    void loop();
    bool isRunning() { return running; }
private:
    Player player;
    bool running;
    TCOD_key_t key;
};